public class Application
{
	public static void main(String[]args)
	{
		Student person = new Student();
		person.studyTime = 3.5;
		person.sleepTime = 7;
		person.education = "Dawson";
		
		System.out.println(person.studyTime);
		System.out.println(person.sleepTime);
		System.out.println(person.education);
		
		Student person2 = new Student();
		person2.studyTime = 9.5;
		person2.sleepTime = 4.3;
		person2.education = "Brebeuf";
		
		double sleep = person.sleep(2);
		
		System.out.println(person2.studyTime);
		System.out.println(person2.sleepTime);
		System.out.println(person2.education);		
		
		person.educationIn("Dawson");
		System.out.println(sleep);
		
		Student[] section4 = new Student[3];
		section4[0] = person;
		section4[1] = person2;
		
		section4[2] = new Student();
		
		section4[2].education = "Brebeuf"; 
		section4[2].studyTime = 4.9;
		section4[2].sleepTime = 5;
		
		System.out.println(section4[2].studyTime);
	}
	
	
}